package modul3A;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Latihan4 extends JFrame {

    public Latihan4() {
        this.setLayout(null);
        this.setSize(300, 150);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);

        JLabel label = new JLabel("Keyword :");
        label.setBounds(118, 10, 60, 20);
        this.add(label);

        JTextField textField = new JTextField(null);
        textField.setBounds(17, 40, 250, 20);
        this.add(textField);

        JButton button = new JButton("Find");
        button.setBounds(116, 80, 60, 20);
        this.add(button);

    }

    public static void main(String[] args) {
        new Latihan4();
    }
}

// PROGRAM LATIHAN 4 MODUL 3A
