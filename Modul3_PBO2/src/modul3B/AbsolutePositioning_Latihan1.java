package modul3B;

import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class AbsolutePositioning_Latihan1 extends JFrame {

    private JLabel label_nama, label_jenisKelamin, label_hobi;
    private JTextField textField;
    private JButton buttonOK, buttonCancel;
    private JRadioButton radioButtonLaki, radioButtonPerempuan;
    private JCheckBox checkOlahraga, checkShopping;
    private JCheckBox checkComputerGames, checkNontonBioskop;

    public AbsolutePositioning_Latihan1() {
        Container contenPan = getContentPane();
        setTitle("Input Data");
        setResizable(false);
        setSize(400, 280);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        contenPan.setLayout(null);

        label_nama = new JLabel("Nama :");
        label_nama.setBounds(20, 20, 60, 20);
        contenPan.add(label_nama);
        textField = new JTextField();
        textField.setBounds(150, 20, 210, 20);
        contenPan.add(textField);

        label_jenisKelamin = new JLabel("Jenis Kelamin :");
        label_jenisKelamin.setBounds(20, 50, 100, 20);
        contenPan.add(label_jenisKelamin);
        radioButtonLaki = new JRadioButton("Laki-Laki");
        radioButtonLaki.setBounds(150, 50, 100, 20);
        contenPan.add(radioButtonLaki);
        radioButtonPerempuan = new JRadioButton("Perempuan");
        radioButtonPerempuan.setBounds(250, 50, 100, 20);
        contenPan.add(radioButtonPerempuan);

        label_hobi = new JLabel("Hobi : ");
        label_hobi.setBounds(20, 80, 60, 20);
        contenPan.add(label_hobi);
        checkOlahraga = new JCheckBox("Olahraga");
        checkOlahraga.setBounds(150, 80, 100, 20);
        contenPan.add(checkOlahraga);
        checkShopping = new JCheckBox("Shopping");
        checkShopping.setBounds(150, 110, 100, 20);
        contenPan.add(checkShopping);
        checkComputerGames = new JCheckBox("Computer Games");
        checkComputerGames.setBounds(150, 140, 180, 20);
        contenPan.add(checkComputerGames);
        checkNontonBioskop = new JCheckBox("Nonton Bioskop");
        checkNontonBioskop.setBounds(150, 170, 180, 20);
        contenPan.add(checkNontonBioskop);

        buttonOK = new JButton("OK");
        buttonOK.setBounds(110, 220, 75, 20);
        contenPan.add(buttonOK);
        buttonCancel = new JButton("Cancel");
        buttonCancel.setBounds(210, 220, 75, 20);
        contenPan.add(buttonCancel);
    }

    public static void main(String[] args) {
        new AbsolutePositioning_Latihan1();
    }
}

// PROGRAM LATIHAN 1 MODUL 3B
