package modul3C;

import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Nomor6 extends JDialog {

    private JLabel labelGambar, labelNamaGambar;
    private JTextArea textArea_deskripsi;
    private JComboBox comboBox_negara;

    public Nomor6() {
        initGambarComboBox();
        initTextAreaScroll();
        setTitle("ComboBoxDemo");
        setSize(450, 300);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    public void initGambarComboBox() {
        this.setLayout(null);
        String[] negara = {"Canada", "English", "Indonesia", "Malaysia"};
        comboBox_negara = new JComboBox(negara);
        comboBox_negara.setBounds(1, 1, 430, 30);
        this.add(comboBox_negara);

        URL canada = this.getClass().getResource("canada.png");
        ImageIcon imageCanada = new ImageIcon(canada);
        labelGambar = new JLabel(imageCanada);
        labelGambar.setBounds(2, 10, 250, 200);
        this.add(labelGambar);

        labelNamaGambar = new JLabel("Canada");
        labelNamaGambar.setBounds(105, 200, 80, 30);
        this.add(labelNamaGambar);
    }

    public void initTextAreaScroll() {
        textArea_deskripsi = new JTextArea("Negara Canada");
        textArea_deskripsi.setBounds(260, 35, 170, 200);
        JScrollPane scrl = new JScrollPane(textArea_deskripsi, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrl.setBounds(260, 35, 170, 200);
        this.add(scrl);
    }

    public static void main(String[] args) {
        new Nomor6();
    }
}
