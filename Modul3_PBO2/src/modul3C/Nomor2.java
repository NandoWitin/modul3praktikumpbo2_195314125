package modul3C;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

public class Nomor2 extends JDialog {

    private JButton buttonYellow, buttonBlue, buttonRed;

    public Nomor2() {
        setTitle("ButtonTest");
        setSize(400, 250);

        JPanel wadah = new JPanel();
        buttonYellow = new JButton("Yellow");
        buttonBlue = new JButton("Blue");
        buttonRed = new JButton("Red");
        wadah.add(buttonYellow);
        wadah.add(buttonBlue);
        wadah.add(buttonRed);
        this.add(wadah);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new Nomor2();
    }
}
