package modul3C;

import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;

public class Nomor7 extends JDialog {

    private JList listNegara;
    private JLabel labelGambar_australia, labelGambar_portugal, labelGambar_jamaika,
            labelGambar_denmark, labelGambar_yaman, labelGambar_jerman;
    String[] negara = {"Indonesia", "Australia", "Denmark", "Portugal", "Jamaica", "Malaysia","Brazil", "Canada", "USA", "Jepang", "Yaman", "Jerman"};

    public Nomor7() {
        setTitle("ListDemo");
        setSize(700, 300);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
        initLabelGambar();
        initListNegara();
    }

    public void initListNegara() {
        listNegara = new JList(negara);
        listNegara.setBounds(1, 1, 200, 100);
        JScrollPane sc = new JScrollPane(listNegara, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        sc.setBounds(1, 1, 200, 170);
        this.add(sc);
    }

    public void initLabelGambar() {
        setLayout(null);
        URL bendera = this.getClass().getResource("australia.png");
        URL bendera1 = this.getClass().getResource("denmark.png");
        URL bendera2 = this.getClass().getResource("jamaika.png");
        URL bendera3 = this.getClass().getResource("jerman.png");
        URL bendera4 = this.getClass().getResource("portugal.png");
        URL bendera5 = this.getClass().getResource("yaman.png");

        ImageIcon image = new ImageIcon(bendera);
        ImageIcon image1 = new ImageIcon(bendera1);
        ImageIcon image2 = new ImageIcon(bendera2);
        ImageIcon image3 = new ImageIcon(bendera3);
        ImageIcon image4 = new ImageIcon(bendera4);
        ImageIcon image5 = new ImageIcon(bendera5);

        labelGambar_australia = new JLabel(image);
        labelGambar_australia.setBounds(210, 1, 150, 100);
        labelGambar_denmark = new JLabel(image1);
        labelGambar_denmark.setBounds(210, 110, 150, 100);
        labelGambar_jamaika = new JLabel(image2);
        labelGambar_jamaika.setBounds(370, 1, 150, 100);
        labelGambar_jerman = new JLabel(image3);
        labelGambar_jerman.setBounds(530, 1, 150, 100);
        labelGambar_portugal = new JLabel(image4);
        labelGambar_portugal.setBounds(370, 110, 150, 100);
        labelGambar_yaman = new JLabel(image5);
        labelGambar_yaman.setBounds(530, 110, 150, 100);

        this.add(labelGambar_australia);
        this.add(labelGambar_denmark);
        this.add(labelGambar_jamaika);
        this.add(labelGambar_jerman);
        this.add(labelGambar_portugal);
        this.add(labelGambar_yaman);
    }

    public static void main(String[] args) {
        new Nomor7();
    }
}
